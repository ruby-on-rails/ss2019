require_relative 'lib/array'
require_relative 'lib/game'
require_relative 'lib/hero'
require_relative 'lib/monster'

h = Hero.new({name: 'Conan', str: 5, con: 3})
monsters = Game::create_monsters

while(h.alive?)
  puts h
  if Game.fight(h, monsters.rand)
    monsters.each { |monster| monster.upgrade }
  end
end
