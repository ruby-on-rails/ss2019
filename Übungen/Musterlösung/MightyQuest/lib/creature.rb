class Creature

  attr_reader :name, :lvl, :exp

  def initialize(options = {})
    @name = options[:name] || "Anonymous Creature"
    @str  = options[:str]  || 1
    @con  = options[:con]  || 1
    @dex  = options[:dex]  || 1
    @int  = options[:int]  || 1
    @lvl  = options[:lvl]  || 1
    @exp  = options[:exp]  || 0
    @hp   = options[:hp]   || 2 * @con + @str
  end

  def alive?
    @hp > 0
  end

  def wound(damage)
    @hp -= damage
  end

  def attack(enemy)
    raise ArgumentError.new("self attack not allowed") if enemy == self
    raise ArgumentError.new("wrong type of enemy") unless enemy.is_a? Creature

    enemy.wound(@str + @str * rand(3))
  end

  def to_s
    <<-EOF.chop
#{@name} Level #{@lvl}
HP = #{@hp}
EXP = #{@exp}
STR = #{@str}
CON = #{@con}
DEX = #{@dex}
INT = #{@int}
    EOF
  end

  def status
    "#{@name}: #{@hp}"
  end
end
