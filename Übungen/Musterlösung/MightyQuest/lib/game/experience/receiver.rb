require_relative '../../array'

module Game::Experience::Receiver
  def gain_xp xp
    @exp += xp
    lvl_up = false
    while @exp >= (@lvl * 10)
      self.levelup
      lvl_up = true
    end
    lvl_up
  end

  def levelup
    @lvl += 1
    attr_points = 5
    attrs = [:@str, :@dex, :@con, :@int]
    while attr_points > 0
      points = rand(1..attr_points)
      attr_points -= points
      attr = attrs.rand
      begin
        self.instance_variable_set(attr, self.instance_variable_get(attr) + points)
      rescue
      end
    end
    begin
      @hp = 2 * @con + @str
    rescue
    end
  end
end
