require_relative './creature.rb'
require_relative 'game/experience'

class Hero < Creature
  include Game::Experience::Receiver
  def to_s
    "Hero\n#{super.to_s}"
  end
end
