require 'faker'

module Game
  require_relative 'hero'
  require_relative 'monster'

  def self.fight(hero, monster)
    while(hero.alive? and monster.alive?)
      puts hero.status
      puts monster.status
      hero.attack(monster)
      if monster.alive?
        monster.attack(hero)
      end
    end
    puts "Hero #{hero.name} #{(hero.alive?)?"wins":"loses"}!"
    hero_lvl_up = false
    if hero.alive?
      hero_lvl_up = hero.gain_xp monster.xp
    end
    hero_lvl_up
  end

  def self.create_monster
    Monster.new({name: Faker::Games::ElderScrolls.creature, str: 1+rand(3), con: 1+rand(3), dex: 1+rand(3), int: 1+rand(3), exp: 1+rand(3)})
  end

  def self.create_monsters
    Array.new(10) {|i| Game::create_monster}
  end
end
