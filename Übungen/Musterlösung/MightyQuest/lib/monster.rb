require_relative './creature.rb'
require_relative 'game/experience'

class Monster < Creature
  include Game::Experience::Source
  include Game::Experience::Receiver

  def upgrade
    self.levelup
  end


  def to_s
    "Monster\n#{super.to_s}"
  end
end
