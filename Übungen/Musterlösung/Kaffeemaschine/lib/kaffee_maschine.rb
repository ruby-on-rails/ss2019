require_relative './exceptions.rb'

class KaffeeMaschine
  CupSize = 1

  def initialize
    @wasserfuellstand = 3
    @online = false
  end

  def einschalten
    @online = true
    puts "Kaffeemaschine ist eingeschaltet!"
  end

  def koche_kaffee(n, drink: :coffee)
    unless @online
      raise NotOnlineError
    end
    if @wasserfuellstand < n * CupSize
      raise NotEnoughWaterLeftError
    end
    @wasserfuellstand -= n * CupSize
    unless [:kaffee, :tee, :himbeer].include? drink
      raise DrinkNotAvailable
    end
    drink_sym = "☕"
    if drink == :tee
      drink_sym = "🍵"
    elsif drink == :himbeer
      drink_sym = "🍺"
    end

    puts ([drink_sym] * n).join ' '
  end
end
